// env.js

const dotenv = require('dotenv');
const env = dotenv.config().parsed;

export default env;
