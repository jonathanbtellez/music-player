import { sveltekit } from '@sveltejs/kit/vite';
import { defineConfig } from 'vite';
import replace from '@rollup/plugin-replace';
import dotenv from 'dotenv';

dotenv.config();

export default defineConfig({
	plugins: [
		sveltekit(),
		replace({
			preventAssignment: true,
			values: {
				'process.env.SPOTIFY_KEY': JSON.stringify(process.env.SPOTIFY_KEY),
			}
		})
	]
});
