import axios from 'axios';
import qs from 'qs';

export const instance = axios.create({
	baseURL: 'https://api.spotify.com.'
});

const client_id = import.meta.env.VITE_CLIENT_ID;
const client_secret = import.meta.env.VITE_CLIENT_SECRET;

const authOptions = {
	url: 'https://accounts.spotify.com/api/token',
	method: 'post',
	headers: {
		// @ts-ignore
		'content-type': 'application/x-www-form-urlencoded'
	},
	data:  qs.stringify({
		'grant_type': 'client_credentials',
		'client_id': client_id,
		'client_secret': client_secret
	  })
};

export const instanceAuth = axios(authOptions);
