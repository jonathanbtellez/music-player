import { instance, instanceAuth } from '../config/intanceSpotify';

export const getFiveSongs = async () => {
    const { access_token } = JSON.parse(localStorage.getItem('token'));

    if(access_token){
        try {
            const { data } = await instance.get('v1/tracks?ids=2uHpC2ZWJqOWU0BkpnPDNg%2C1yYr4D1KjhhyDD5bZIiFQy%2C4eJn7EntiIgv6Jl1eR1uZT%2C1yKAqZoi8xWGLCf5vajroL%2C6zKF4293k44ItKWJJgrhXv', {
                headers: {
                    Authorization: `Bearer ${access_token}`,
                },
            });
            return data.tracks;
        } catch (e) {
            localStorage.clear();
            console.error(e);
			await authSpotify();
            getFiveSongs()
        }
    }
};

export const authSpotify = async () => {
	try {
        const token  = JSON.parse(localStorage.getItem('token'));

        if(!token){
            const response = await instanceAuth;
            localStorage.setItem('token', JSON.stringify(response.data));
        }
	} catch (e) {
		console.error(e);
	}
};
